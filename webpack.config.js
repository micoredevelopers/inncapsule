const JsPlugins = require('./webpack/Js')
const CssPlugins = require('./webpack/Css')
const SassPlugins = require('./webpack/Sass')
const CopyPlugins = require('./webpack/Copy')
const CleanPlugins = require('./webpack/Clean')
const TesterPlugins = require('./webpack/Tester')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  output: { filename: './js/bundle.js' },
  entry: ['./src/js/index.js', './src/scss/style.scss'],
  module: { rules: [JsPlugins, SassPlugins] },
  optimization: { minimizer: [TesterPlugins()] },
  plugins: [CleanPlugins(), CssPlugins(), CopyPlugins(), new HtmlWebpackPlugin({
    filename: 'index.html',
    template: 'src/index.html'
  })]
}
