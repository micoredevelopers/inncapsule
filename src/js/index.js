import $ from 'jquery'
import 'slick-carousel'
import AOS from 'aos'
import mask from 'jquery.maskedinput/src/jquery.maskedinput'
import 'bootstrap/dist/js/bootstrap.bundle.min'

/* INIT FUNCTIONS */
/* CONVERTS IMAGES INTO SVG */
$('img.svg').each(function () {
  const $img = $(this)
  const imgID = $img.attr('id')
  const imgClass = $img.attr('class')
  const imgURL = $img.attr('src')
  
  $.get(imgURL, function (data) {
    let $svg = $(data).find('svg')
    
    if (!$svg.attr('viewBox')) {
      $svg.attr('viewBox', (`0 0 ${$svg.attr('width').match(/[0-9]+\.[0-9]*/)} ${$svg.attr('height').match(/[0-9]+\.[0-9]*/)}`))
    }
    
    if (typeof imgID !== 'undefined') {
      $svg = $svg.attr('id', imgID)
    }
    
    if (typeof imgClass !== 'undefined') {
      $svg = $svg.attr('class', imgClass + ' replaced-svg')
    }
    
    $svg = $svg.removeAttr('xmlns:a')
    $img.replaceWith($svg)
  }, 'xml')
})

/* AOS INITIALIZATION */
AOS.init({ once: true, duration: 700, easing: 'default-bezier' })

/* GET RANDOM NUMBER FUNC */
const randomIntFromInterval = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)

/* CONVERT DATE INTO UNIX FORMAT */
const toTimeStamp = (date) => new Date(date).getTime()

/* GET CURRENT DATE AND PASTE INTO INPUTS VALUE ON MAIN SECTION */
const Today = new Date().toJSON().slice(0, 10)
const Tomorrow = new Date(Today)
Tomorrow.setDate(Tomorrow.getDate() + 1)
$('#arrival-date, #reservationArrival').val(Today)
$('#departure-date, #reservationDeparture').val(Tomorrow.toJSON().slice(0, 10))

/* ANIMATE GRID RECTANGLES ON MAIN SECTION */
setTimeout(function () {
  const gridG = $('.grid-blocks>g>g>g')
  
  clearInterval(int)
  const int = setInterval(() => {
    const randIndex = randomIntFromInterval(4, 7)
    
    gridG.removeClass('active')
    
    for (let i = 0; i < randIndex; i++) {
      const randG = gridG[Math.floor(Math.random() * gridG.length)]
      const randDelay = randomIntFromInterval(0, 1800)
      
      $(randG).css('transition-delay', `${randDelay}ms`)
      $(randG).addClass('active')
    }
  }, 3000)
}, 500)

/* INIT SLIDERS */
$('#capsule-slider').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 1,
  dots: false,
  prevArrow: `<div class="slide-arrow prev"><img src="../img/SlideArrow.svg" alt="Arrow"></div>`,
  nextArrow: `<div class="slide-arrow next"><img src="../img/SlideArrow.svg" alt="Arrow"></div>`,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1
      }
    }
  ]
})
$('#gallery-slider').slick({
  infinite: true,
  dots: false,
  slidesToShow: 2,
  slidesToScroll: 1,
  prevArrow: `<div class="slide-arrow prev" data-aos="fade-up-20" data-aos-delay="1800"><img class="svg" src="../img/GalleryArrow.svg" alt="Arrow"></div>`,
  nextArrow: `<div class="slide-arrow next" data-aos="fade-up-20" data-aos-delay="1950"><img class="svg" src="../img/GalleryArrow.svg" alt="Arrow"></div>`,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1
      }
    }
  ]
})

/* ANIMATE TITLE ON MAIN PAGE (SPLIT WORD INTO SPAN AND ANIMATE EACH ONE'S) */
let delay = 0
const mainTitle = $('#main .section-title')
const wordToSplit = mainTitle.attr('data-word').split('')
wordToSplit.map(letter => {
  delay = delay + 30
  mainTitle.append(`<span style="animation: letter-fade 30ms cubic-bezier(1, 0.24, 0, 0.97) both; animation-delay: ${delay}ms">${letter}</span>`)
})

/* ADD VERTICAL/HORIZONTAL LINES TO ELEMENTS */
const lineItems = $('[data-line]')
lineItems.each((index, item) => {
  const lineItem = $(item)
  const wrapBox = $(`<div class="line-wrap"></div>`)
  const noWrap = lineItem.attr('data-no-wrap')
  const lineDirection = lineItem.attr('data-line-position').split(' ')
  
  if (lineDirection) {
    lineDirection.map(direction => {
      switch (direction) {
        case 'top':
          $(noWrap === '' ? lineItem : wrapBox).append(`<span class="line line__top"></span>`)
          break
        case 'bottom':
          $(noWrap === '' ? lineItem : wrapBox).append(`<span class="line line__bottom"></span>`)
          break
        case 'left':
          $(noWrap === '' ? lineItem : wrapBox).append(`<span class="line line__left"></span>`)
          break
        case 'right':
          $(noWrap === '' ? lineItem : wrapBox).append(`<span class="line line__right"></span>`)
          break
      }
    })
  }
  
  if (noWrap !== '') {
    lineItem.before(wrapBox, lineItem)
    wrapBox.append(lineItem)
  }
})

/* INIT MASK INPUT IN MODAL RESERVATION */
$('#userPhone, #phone').mask('+38 (099) 999-99-99')

/* EVENTS FUNCTIONS */
/* ON SCROLL FUNCTION TO HIDE/SHOW LAYER ON HEADER WHEN WE SCROLL TO ABOUT SECTION */
$(window).on('scroll', function () {
  const offsetTop = window.pageYOffset
  const topPos = $('html').offset().top
  
  if (offsetTop > topPos) {
    $('header').addClass('active')
  } else {
    $('header').removeClass('active')
  }
})

/* MENU BURGER ANIMATION AND DISABLE SCROLL FOR SITE */
$('.menu-burger').on('click', function () {
  $(this).toggleClass('active')
  $('header').addClass('active')
  $('.menu-container').toggleClass('active')
  $('html, body').toggleClass('is-overflow')
})

/* SCROLL ANIMATION TO EACH SECTION IN HEADER LINKS AND MENU LINKS*/
$('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click(function (e) {
  $('.menu-burger').removeClass('active')
  $('.menu-container').removeClass('active')
  $('html, body').removeClass('is-overflow')
  
  if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
    let target = $(this.hash)
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']')
    
    if (target.length) {
      e.preventDefault()
      
      setTimeout(() => {
        $('html, body').animate({
          scrollTop: target.offset().top - 60
        }, 1000)
      }, 500)
    }
  }
})

/* WORK WITH CAPSULE MODULE API (GET PRICES, CATEGORIES AND RESERVATION) */
/* ALL API URLS */
const API_KEY = '1207a3b6-eeb4-4dc7-ab10-f068aa824853'
const API_URL = `https://my.easyms.co/api/reservation/pub/${API_KEY}`
const API_PRICES = `${API_URL}/prices`
const API_CATEGORIES = `${API_URL}/availableCategories`
const API_CATEGORIES_LIST = `${API_URL}/categoryDescriptions`
const API_RESERVATION = `${API_URL}/createOrder`

const dateOptions = {
  year: 'numeric',
  month: 'long',
  day: 'numeric',
  weekday: 'short'
}

/* API CALLS */
const getPrices = ({ arrival, departure }) => $.ajax({
  method: 'GET',
  url: `${API_PRICES}?startTime=${arrival}&endTime=${departure}`
}).done(res => res)

const getCategories = () => $.ajax({
  method: 'GET',
  url: API_CATEGORIES_LIST
}).done(res => res)

const availableCategories = ({ arrival, departure }) => $.ajax({
  method: 'GET',
  url: `${API_CATEGORIES}?startTime=${arrival}&endTime=${departure}`
}).done(res => res)

/* ELEMENTS */
const capsuleCont = $('.capsule-row')
const btnContinue = $('.btn-continue')
const successModal = $('#modalReservationSuccess')
const reservationModal = $('#modalReservation')
const nightsText = $('.reservation-date.count span')
const arrivalField = $('.reservation-date.arrival')
const departureField = $('.reservation-date.departure')
const arrivalText = arrivalField.find('span')
const departureText = departureField.find('span')
const totalField = $('.reservation-total-text.total')
const infoContainer = $('.reservation-capsule-info-container')
const capsuleRows = $('.col-capsule')
const submitForm = $('.col-submit-form')
const reservationForm = $('.col-reservation-form')
const headerTitleMain = $('.header-title.main')
const headerTitleBack = $('.header-title.back')
const footerCollapse = $('#reservationInfo')
const userInfo = $('.col-user-info')
const modalFooter = $('.modal-footer')

if (window.innerWidth > 576) {
  footerCollapse.collapse('show')
}

const sendForm = (data) => {
  try {
    $.ajax({
      method: 'POST',
      url: API_RESERVATION,
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      data: JSON.stringify(data)
    })
    $('#modalReservation').modal('hide')
    setTimeout(() => {
      successModal.modal('show')
    }, 600)
  } catch (e) {
    alert(e)
  }
}

const goToMainForm = () => {
  capsuleRows.removeClass('d-none')
  reservationForm.removeClass('d-none')
  submitForm.addClass('d-none')
  btnContinue.attr('type', 'button')
  btnContinue.find('span').text('продолжить')
  headerTitleMain.removeClass('d-none')
  headerTitleBack.addClass('d-none')
  userInfo.addClass('col-lg-4')
  userInfo.removeClass('col-lg-12')
  modalFooter.addClass('col-lg-12')
  modalFooter.removeClass('col-lg-4')
}

const goToSecondForm = () => {
  capsuleRows.addClass('d-none')
  reservationForm.addClass('d-none')
  submitForm.removeClass('d-none')
  headerTitleMain.addClass('d-none')
  headerTitleBack.removeClass('d-none')
  userInfo.removeClass('col-lg-4')
  userInfo.addClass('col-lg-12')
  modalFooter.removeClass('col-lg-12')
  modalFooter.addClass('col-lg-4')
}

/* SUBMIT ON MAIN FORM TO OPEN MODAL WITH CAPSULE RESERVATION */
$('#data-reservation-form, #main-form').on('submit', async function (e) {
  e.preventDefault()
  const data = {}
  const inputsVal = {}
  const inputs = $(this).find('.calendar-input')
  
  reservationModal.modal('show')
  arrivalText.text('')
  departureText.text('')
  nightsText.text('')
  infoContainer.html('')
  totalField.text('UAH 0')
  capsuleCont.html('')
  inputs.each((index, input) => {
    const date = $(input).val()
    const name = $(input).attr('name')
    
    inputsVal[name] = date
    data[name] = toTimeStamp(date)
  })
  
  $('#reservationArrival').val(inputsVal.arrival)
  $('#reservationDeparture').val(inputsVal.departure)
  
  const categories = await getCategories()
  const pricesInfo = await getPrices({
    arrival: data.arrival,
    departure: data.departure
  })
  const avCategories = await availableCategories({
    arrival: data.arrival,
    departure: data.departure
  })
  
  avCategories.length === 0 ?
    capsuleCont.append(`<div class="col-12"><h5 class="text-not-found">Нет свободных капсул!</h5></div>`) :
    categories.map(description => {
      avCategories.map((available, index) => {
        pricesInfo.prices.map((price) => {
          if (description.categoryId === available.categoryId && available.categoryId === price.categoryId) {
            const cutName = available.categoryName.replace('\'', '')
            
            capsuleCont.append(`
            <div class="col-12 capsule-col">
              <div class="reservation-capsule-card">
                <div class="row">
                  <div class="col-12 col-md-5">
                    <div class="image-wrap reservation-capsule-image">
                      <img src=${description.images[0]} alt="Capsule image">
                    </div>
                  </div>
                  <div class="col-12 col-md-7">
                    <div class="capsule-info-container">
                      <h2 class="reservation-capsule-name">${available.categoryName}</h2>
                      <p class="reservation-capsule-available">Доступно: <span>${available.availability}</span></p>
                      <p class="reservation-capsule-desc">${description.description}</p>
                      <p class="reservation-capsule-price">
                        <span class="currency">${pricesInfo.currency} </span><span class="price">${price.value}</span>
                      </p>
                      <button type="button" class="custom-btn reservation-capsule-btn" data-price=${price.value}
                      data-count=${available.availability}>
                        <span>забронировать</span>
                      </button>
                      <div class="reservation-select-group d-none">
                        <div class="form-select-group">
                          <label for="capsule-count" class="select-label">Укажите количество:</label>
                          <select name="capsule-count" id="capsule-count-${index}" class="form-select"
                          data-name=${cutName} data-full-name=${available.categoryName} data-price=${price.value} data-category-id=${available.categoryId}></select>
                        </div>
                        <button class="custom-btn btn-cancel" data-name=${cutName} type="button">Отмена</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          `)
          }
        })
      })
    })
  
  const nDArr = new Date(inputsVal.arrival).toLocaleString('ru', dateOptions)
  const nDDep = new Date(inputsVal.departure).toLocaleString('ru', dateOptions)
  const nightsCount = new Date(inputsVal.departure).getDate() - new Date(inputsVal.arrival).getDate()
  
  arrivalText.text(nDArr)
  departureText.text(nDDep)
  nightsText.text(nightsCount)
  arrivalField.attr('data-date', data.arrival)
  departureField.attr('data-date', data.departure)
  
  /* CAPSULE CARD EVENT ON BUTTON */
  $('.reservation-capsule-btn').on('click', function () {
    const count = $(this).attr('data-count')
    const price = $(this).attr('data-price')
    const selectGroup = $(this).parent().find('.reservation-select-group')
    const select = $(this).parent().find('.form-select')
    
    $(this).addClass('d-none')
    selectGroup.removeClass('d-none')
    
    select.html('')
    select.append('<option value="0" selected disabled>Кол-во капсул</option>')
    
    for (let i = 0; i < count; i++) {
      select.append(`<option value=${i + 1}>${i + 1} (${pricesInfo.currency} ${Number(price) * Number(i + 1)})</option>`)
    }
  })
  
  /* CAPSULE CARD EVENT ON CHANGE CAPSULE COUNT */
  $('.form-select').on('change', function () {
    let allSum = 0
    const count = $(this).val()
    const price = $(this).attr('data-price')
    const id = $(this).attr('data-category-id')
    const categoryName = $(this).attr('data-name')
    const categoryFullName = $(this).attr('data-full-name')
    const selectedInfoRow = $(`.reservation-capsule-info[data-name=${categoryName}]`)
    const totalSum = Number(count) * Number(price)
    
    btnContinue.attr('disabled', false)
    
    if (selectedInfoRow) {
      selectedInfoRow.remove()
    }
    
    infoContainer.append(`
        <div class="reservation-capsule-info" data-id=${id} data-name=${categoryName} data-count=${totalSum}>
          <h2 class="capsule-name">${categoryFullName}</h2>
          <div class="bottom-row">
            <p class="capsule-counts">Номеров: ${count} x ${price} ${pricesInfo.currency}</p>
            <p class="capsule-sum">${totalSum} ${pricesInfo.currency}</p>
          </div>
        </div>
      `)
    
    $('.reservation-capsule-info').each((index, row) => {
      const sum = $(row).attr('data-count')
      allSum += Number(sum)
    })
    
    totalField.text(`${pricesInfo.currency} ${allSum}`)
  })
  
  /* EVENT ON CANCEL BUTTON */
  $('.btn-cancel').on('click', function () {
    let allSum = 0
    const name = $(this).attr('data-name')
    const selectedInfoRow = $(`.reservation-capsule-info[data-name=${name}]`)
    const reservationBtn = $(this).parents('.capsule-info-container').find('.reservation-capsule-btn')
    
    reservationBtn.removeClass('d-none')
    $(this).parent().addClass('d-none')
    selectedInfoRow.remove()
    
    $('.reservation-capsule-info').each((index, row) => {
      const sum = $(row).find('.capsule-sum').text().split(' ')[0]
      
      allSum += Number(sum)
    })
    
    totalField.text(`${pricesInfo.currency} ${allSum}`)
    
    if (allSum === 0) {
      btnContinue.attr('disabled', true)
    }
  })
})

headerTitleBack.on('click', function () {
  goToMainForm()
})

btnContinue.on('click', function () {
  goToSecondForm()
  
  $(this).attr('type', 'submit')
  $(this).find('span').text('забронировать')
  
  if (window.innerWidth < 576) {
    footerCollapse.collapse('hide')
  }
})

$('#submit-form').on('submit', function (e) {
  e.preventDefault()
  const userData = {}
  const capsuleData = []
  const formInputs = $(this).find('.form-input')
  const capsuleInfo = $('.reservation-capsule-info')
  const dateArrival = $('.reservation-date.arrival').attr('data-date')
  const dateDeparture = $('.reservation-date.departure').attr('data-date')
  
  formInputs.each((index, input) => {
    const val = $(input).val()
    const name = $(input).attr('name')
    
    userData[name] = val
  })
  
  capsuleInfo.each((item, capsule) => {
    const id = $(capsule).attr('data-id')
    const total = $(capsule).attr('data-count')
    
    capsuleData.push({
      categoryId: Number(id),
      invoice: Number(total),
      arrival: Number(dateArrival),
      departure: Number(dateDeparture)
    })
  })
  
  sendForm({ customer: userData, rooms: capsuleData })
})

$('.btn-new-search').on('click', function () {
  successModal.modal('hide')
  setTimeout(() => {
    reservationModal.modal('show')
  }, 600)
})

reservationModal.on('hidden.bs.modal', function () {
  $(this).find('#submit-form .form-input').val('')
})

reservationModal.on('show.bs.modal', function () {
  goToMainForm()
})

$('#modalFeedbackFailure').on('hidden.bs.modal', function () {
  $(this).find('.errors-container').html('')
})

/* CONTACT SEND SUBMIT */
$('#feedback-form').on('submit', function (e) {
  e.preventDefault()
  const userData = {}
  const success = $('#modalFeedbackSuccess')
  const failure = $('#modalFeedbackFailure')
  const errorsBox = $('.errors-container')
  const form = $(this)
  const dataUrl = form.attr('action')
  const formInputs = form.find('.form-input')
  
  formInputs.each((index, input) => {
    const val = $(input).val()
    const id = $(input).attr('id')
    
    userData[id] = val
  })
  
  try {
    $.ajax({
      method: 'POST',
      url: dataUrl,
      data: userData
    })
    .done(function (res) {
      if (res.status === 'success') {
        success.modal('show')
      }
    })
    .fail(function (res) {
      if (res.status === 'error') {
        res.errors && res.errors.map(err => {
          errorsBox.append(err)
        })
      
        failure.modal('show')
      }
    })
  } catch (e) {
    console.log(e)
  }
})
